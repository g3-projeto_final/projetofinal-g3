class CellTasksController < ApplicationController
  before_action :set_cell_task, only: [:show, :edit, :update, :destroy, :task_status]
  before_action :prevent_not_logged_user

  # GET /cell_tasks
  # GET /cell_tasks.json
  def index
    @cell_tasks = CellTask.all
   
  end

  # GET /cell_tasks/1
  # GET /cell_tasks/1.json
  def show
  end

  # GET /cell_tasks/new
  def new
    @cell_task = CellTask.new
  end

  # GET /cell_tasks/1/edit
  def edit
  end

  # POST /cell_tasks
  # POST /cell_tasks.json
  def create
    @cell_task = CellTask.new(cell_task_params)
    @cell_task.done = false

    respond_to do |format|
      if @cell_task.save
        format.html { redirect_to @cell_task.cell, notice: 'Cell task was successfully created.' }
        format.json { render :show, status: :created, location: @cell_task }
      else
        format.html { render :new }
        format.json { render json: @cell_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cell_tasks/1
  # PATCH/PUT /cell_tasks/1.json
  def update
    respond_to do |format|
      if @cell_task.update(cell_task_params)
        format.html { redirect_to @cell_task.cell, notice: 'Cell task was successfully updated.' }
        format.json { render :show, status: :ok, location: @cell_task }
      else
        format.html { render :edit }
        format.json { render json: @cell_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cell_tasks/1
  # DELETE /cell_tasks/1.json
  def destroy
    @cell_task.destroy
    respond_to do |format|
      format.html { redirect_to cell_tasks_url, notice: 'Cell task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cell_task
      @cell_task = CellTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cell_task_params
      params.require(:cell_task).permit(:name, :cell_id)
    end
end
