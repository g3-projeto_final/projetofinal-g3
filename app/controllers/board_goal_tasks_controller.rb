class BoardGoalTasksController < ApplicationController
  before_action :set_board_goal_task, only: [:show, :edit, :update, :destroy]

  # GET /board_goal_tasks
  # GET /board_goal_tasks.json
  def index
    @board_goal_tasks = BoardGoalTask.all
  end

  # GET /board_goal_tasks/1
  # GET /board_goal_tasks/1.json
  def show
  end

  # GET /board_goal_tasks/new
  def new
    @board_goal_task = BoardGoalTask.new
  end

  # GET /board_goal_tasks/1/edit
  def edit
  end

  # POST /board_goal_tasks
  # POST /board_goal_tasks.json
  def create
    @board_goal_task = BoardGoalTask.new(board_goal_task_params)

    respond_to do |format|
      if @board_goal_task.save
        format.html { redirect_to @board_goal_task, notice: 'Board goal task was successfully created.' }
        format.json { render :show, status: :created, location: @board_goal_task }
      else
        format.html { render :new }
        format.json { render json: @board_goal_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /board_goal_tasks/1
  # PATCH/PUT /board_goal_tasks/1.json
  def update
    respond_to do |format|
      if @board_goal_task.update(board_goal_task_params)
        format.html { redirect_to @board_goal_task, notice: 'Board goal task was successfully updated.' }
        format.json { render :show, status: :ok, location: @board_goal_task }
      else
        format.html { render :edit }
        format.json { render json: @board_goal_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /board_goal_tasks/1
  # DELETE /board_goal_tasks/1.json
  def destroy
    @board_goal_task.destroy
    respond_to do |format|
      format.html { redirect_to board_goal_tasks_url, notice: 'Board goal task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board_goal_task
      @board_goal_task = BoardGoalTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def board_goal_task_params
      params.require(:board_goal_task).permit(:boardgoal_id)
    end
end
