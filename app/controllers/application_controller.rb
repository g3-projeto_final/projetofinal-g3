class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to root_path, notice: 'Você não tem permissão para acessar essa página' }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  def after_sign_in_path_for(resource)
    @user
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :kind, :photo, :board_id])
      devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :kind, :photo])
    end

    def prevent_not_logged_user
      redirect_to new_user_session_path if current_user.nil?
    end

    def prevent_if_not_minimum(kind)
      if current_user && current_user.kind_before_type_cast < kind
        flash[:notice] = 'Você não tem permissão para acessar essa página.'
        redirect_to root_path
      end
    end
end
