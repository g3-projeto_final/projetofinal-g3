class HomeController < ApplicationController
  def home
    if current_user.nil?
      redirect_to new_user_session_path
    else
      redirect_to current_user
    end
  end
end
