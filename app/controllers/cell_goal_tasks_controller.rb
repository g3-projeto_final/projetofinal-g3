class CellGoalTasksController < ApplicationController
  before_action :set_cell_goal_task, only: [:show, :edit, :update, :destroy]

  # GET /cell_goal_tasks
  # GET /cell_goal_tasks.json
  def index
    @cell_goal_tasks = CellGoalTask.all
  end

  # GET /cell_goal_tasks/1
  # GET /cell_goal_tasks/1.json
  def show
  end

  # GET /cell_goal_tasks/new
  def new
    @cell_goal_task = CellGoalTask.new
  end

  # GET /cell_goal_tasks/1/edit
  def edit
  end

  # POST /cell_goal_tasks
  # POST /cell_goal_tasks.json
  def create
    @cell_goal_task = CellGoalTask.new(cell_goal_task_params)

    respond_to do |format|
      if @cell_goal_task.save
        format.html { redirect_to @cell_goal_task.cell_goal, notice: 'Cell goal task was successfully created.' }
        format.json { render :show, status: :created, location: @cell_goal_task }
      else
        format.html { render :new }
        format.json { render json: @cell_goal_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cell_goal_tasks/1
  # PATCH/PUT /cell_goal_tasks/1.json
  def update
    respond_to do |format|
      if @cell_goal_task.update(cell_goal_task_params)
        format.html { redirect_to @cell_goal_task, notice: 'Cell goal task was successfully updated.' }
        format.json { render :show, status: :ok, location: @cell_goal_task }
      else
        format.html { render :edit }
        format.json { render json: @cell_goal_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cell_goal_tasks/1
  # DELETE /cell_goal_tasks/1.json
  def destroy
    @cell_goal_task.destroy
    respond_to do |format|
      format.html { redirect_to cell_goal_tasks_url, notice: 'Cell goal task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cell_goal_task
      @cell_goal_task = CellGoalTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cell_goal_task_params
      params.require(:cell_goal_task).permit(:cell_goal_id, :task_name)
    end
end
