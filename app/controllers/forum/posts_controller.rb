class Forum::PostsController < ApplicationController
  before_action :set_post, only: [:show, :destroy]
  before_action :prevent_not_logged_user

  def index
    @posts = ForumPost.all
  end

  def new
    @post = ForumPost.new
  end

  def show
    redirect_to forum_posts_path if @post.nil?
  end

  def create
    @post = ForumPost.new(forum_post_params)
    @post.user = current_user
    if @post.save
      redirect_to forum_posts_path
    else
      render :new
    end
  end

  def destroy
    @post.destroy
    redirect_to forum_posts_path
  end

  private

    def set_post
      @post = ForumPost.find_by id: params[:id]
    end

    def forum_post_params
      params.require(:forum_post).permit(:title, :content)
    end
end
