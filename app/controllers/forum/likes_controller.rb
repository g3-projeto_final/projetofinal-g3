class Forum::LikesController < ApplicationController
  before_action :prevent_not_logged_user
  before_action :set_forum_post, only: [:create_like, :create_dislike]

  def create_like
    user_reaction = Like.where(user: current_user, forum_post: @forum_post).first

    if user_reaction.nil?
      new_reaction = Like.new user: current_user, forum_post: @forum_post, kind: Like.kinds[:positive]
      new_reaction.save
    elsif user_reaction.positive?
      user_reaction.destroy
    elsif user_reaction.negative?
      user_reaction.destroy
      new_reaction = Like.new user: current_user, forum_post: @forum_post, kind: Like.kinds[:positive]
      new_reaction.save
    end

    redirect_to @forum_post
  end

  def create_dislike
    user_reaction = Like.where(user: current_user, forum_post: @forum_post).first

    if user_reaction.nil?
      new_reaction = Like.new user: current_user, forum_post: @forum_post, kind: Like.kinds[:negative]
      new_reaction.save
    elsif user_reaction.negative?
      user_reaction.destroy
    elsif user_reaction.positive?
      user_reaction.destroy
      new_reaction = Like.new user: current_user, forum_post: @forum_post, kind: Like.kinds[:negative]
      new_reaction.save
    end

    redirect_to @forum_post
  end

  private

    def set_forum_post
      @forum_post = ForumPost.find_by id: params[:post_id]
    end

    def prevent_not_logged_user
      redirect_to new_user_session_path if current_user.nil?
    end
end
