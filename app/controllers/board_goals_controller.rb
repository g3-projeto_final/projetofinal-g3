class BoardGoalsController < ApplicationController
  before_action :set_board_goal, only: [:show, :edit, :update, :destroy]
  before_action :set_board, only: [:edit]

  # GET /board_goals
  # GET /board_goals.json
  def index
    @board_goals = BoardGoal.all
  end

  # GET /board_goals/1
  # GET /board_goals/1.json
  def show
  end

  # GET /board_goals/new
  def new
    @board_goal = BoardGoal.new
    @board = Board.find_by id: params[:board_id]
  end

  # GET /board_goals/1/edit
  def edit
  end

  # POST /board_goals
  # POST /board_goals.json
  def create
    @board_goal = BoardGoal.new(board_goal_params)
    @board = Board.find_by id: params[:board_id]
    @board_goal.board = @board

    respond_to do |format|
      if @board_goal.save
        format.html { redirect_to @board_goal.board, notice: 'Meta de diretoria criada com sucesso.' }
        format.json { render :show, status: :created, location: @board_goal }
      else
        format.html { render :new }
        format.json { render json: @board_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /board_goals/1
  # PATCH/PUT /board_goals/1.json
  def update
    respond_to do |format|
      if @board_goal.update(board_goal_params)
        format.html { redirect_to @board_goal.board, notice: 'Meta de diretoria editada com sucesso.' }
        format.json { render :show, status: :ok, location: @board_goal }
      else
        format.html { render :edit }
        format.json { render json: @board_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /board_goals/1
  # DELETE /board_goals/1.json
  def destroy
    @board_goal.destroy
    respond_to do |format|
      format.html { redirect_to board_goals_url, notice: 'Meta de diretoria excluida com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board
      @board = Board.find_by id: params[:board_id]
      redirect_to boards_path if @board.nil?
    end

    def set_board_goal
      @board_goal = BoardGoal.find_by(board_id: params[:board_id], id: params[:goal_id])
      redirect_to boards_path if @board_goal.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def board_goal_params
      params.require(:board_goal).permit(:name)
    end
end
