class UsersController < ApplicationController
  include ActiveModel::Dirty
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :prevent_not_logged_user

  define_attribute_methods :board_id

  # GET /users
  # GET /users.json
  def index
    prevent_if_not_minimum User.kinds[:admin]
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    prevent_if_not_minimum User.kinds[:admin] if @user != current_user
    if @user.nil?
      redirect_to users_path
      return
    end

    @user_join = Join.where(user_id: @user.id)
    @user_developments = Develop.where({user_id: @user.id})
  end

  # GET /users/new
  # def new
  #   @user = User.new
  # end

  # GET /users/1/edit
  def edit
    prevent_if_not_minimum User.kinds[:admin] if @user != current_user
  end

  # POST /users
  # POST /users.json
  # def create
  #   @user = User.new(user_params)
  #   @user.board = Board.first

  #   respond_to do |format|
  #     if @user.save
  #       format.html { redirect_to @user, notice: 'User was successfully created.' }
  #       format.json { render :show, status: :created, location: @user }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @user.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    prevent_if_not_minimum User.kinds[:admin] if @user != current_user
    board_id_will_change!
    @user.changed?
    respond_to do |format|
      if @user.update(user_params)
        # changes_applied!
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    prevent_if_not_minimum User.kinds[:admin]
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find_by(id: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :kind, :photo)
    end
end
