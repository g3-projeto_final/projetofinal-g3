class CellGoalsController < ApplicationController
  before_action :set_cell_goal, only: [:show, :edit, :update, :destroy]

  # GET /cell_goals
  # GET /cell_goals.json
  def index
    @cell_goals = CellGoal.all
  end

  # GET /cell_goals/1
  # GET /cell_goals/1.json
  def show
  end

  # GET /cell_goals/new
  def new
    @cell_goal = CellGoal.new
  end

  # GET /cell_goals/1/edit
  def edit
  end

  # POST /cell_goals
  # POST /cell_goals.json
  def create
    @cell_goal = CellGoal.new(cell_goal_params)

    respond_to do |format|
      if @cell_goal.save
        format.html { redirect_to @cell_goal, notice: 'Cell goal was successfully created.' }
        format.json { render :show, status: :created, location: @cell_goal }
      else
        format.html { render :new }
        format.json { render json: @cell_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cell_goals/1
  # PATCH/PUT /cell_goals/1.json
  def update
    respond_to do |format|
      if @cell_goal.update(cell_goal_params)
        format.html { redirect_to @cell_goal, notice: 'Cell goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @cell_goal }
      else
        format.html { render :edit }
        format.json { render json: @cell_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cell_goals/1
  # DELETE /cell_goals/1.json
  def destroy
    @cell_goal.destroy
    respond_to do |format|
      format.html { redirect_to cell_goals_url, notice: 'Cell goal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cell_goal
      @cell_goal = CellGoal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cell_goal_params
      params.require(:cell_goal).permit(:cell_id, :goal_name)
    end
end
