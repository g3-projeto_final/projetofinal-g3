class BoardTasksController < ApplicationController
  before_action :set_board_task, only: [:show, :edit, :update, :destroy]
  before_action :set_board, only: [:edit]

  def index
    @board_tasks = BoardTask.all
  end

  def show
  end

  def new
    @board_task = BoardTask.new
    @board = Board.find_by id: params[:board_id]
  end

  # GET /board_tasks/:board_id/tasks/:task_id/edit
  def edit
  end

  def create
    @board_task = BoardTask.new(new_board_task_params)
    @board = Board.find_by id: params[:board_id]
    @board_task.board = @board

    respond_to do |format|
      if @board_task.save
        format.html { redirect_to @board_task.board, notice: 'Tarefa da diretoria criada com sucesso.' }
        format.json { render :show, status: :created, location: @board_task }
      else
        format.html { render :new }
        format.json { render json: @board_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /board_tasks/1
  # PATCH/PUT /board_tasks/1.json
  def update
    respond_to do |format|
      if @board_task.update(edit_board_task_params)
        format.html { redirect_to @board_task.board, notice: 'Tarefa da diretoria criada com sucesso.' }
        format.json { render :show, status: :ok, location: @board_task.board }
      else
        format.html { render :edit }
        format.json { render json: @board_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /board_tasks/1
  # DELETE /board_tasks/1.json
  def destroy
    @board_task.destroy
    respond_to do |format|
      format.html { redirect_to board_tasks_url, notice: 'Board task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_board
      @board = Board.find_by id: params[:board_id]
      redirect_to boards_path if @board.nil?
    end

    def set_board_task
      @board_task = BoardTask.find_by(board_id: params[:board_id], id: params[:task_id])
      redirect_to boards_path if @board_task.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def new_board_task_params
      params.require(:board_task).permit(:name)
    end

    def edit_board_task_params
      params.require(:board_task).permit(:name, :done)
    end
end
