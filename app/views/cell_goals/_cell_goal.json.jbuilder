json.extract! cell_goal, :id, :cell_id, :created_at, :updated_at
json.url cell_goal_url(cell_goal, format: :json)
