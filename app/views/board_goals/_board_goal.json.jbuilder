json.extract! board_goal, :id, :board_id, :created_at, :updated_at
json.url board_goal_url(board_goal, format: :json)
