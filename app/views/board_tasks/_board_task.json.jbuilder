json.extract! board_task, :id, :board_id, :created_at, :updated_at
json.url board_task_url(board_task, format: :json)
