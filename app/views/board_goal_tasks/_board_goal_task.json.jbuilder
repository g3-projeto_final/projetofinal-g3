json.extract! board_goal_task, :id, :boardgoal_id, :created_at, :updated_at
json.url board_goal_task_url(board_goal_task, format: :json)
