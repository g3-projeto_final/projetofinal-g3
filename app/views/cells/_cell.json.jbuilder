json.extract! cell, :id, :name, :board_id, :created_at, :updated_at
json.url cell_url(cell, format: :json)
