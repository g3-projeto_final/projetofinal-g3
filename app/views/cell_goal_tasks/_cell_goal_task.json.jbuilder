json.extract! cell_goal_task, :id, :cellgoal_id, :created_at, :updated_at
json.url cell_goal_task_url(cell_goal_task, format: :json)
