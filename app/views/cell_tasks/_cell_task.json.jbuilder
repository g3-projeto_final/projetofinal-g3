json.extract! cell_task, :id, :name, :cell_id, :created_at, :updated_at
json.url cell_task_url(cell_task, format: :json)
