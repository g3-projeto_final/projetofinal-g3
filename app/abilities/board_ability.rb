# frozen_string_literal: true

class BoardAbility
  include CanCan::Ability
  def initialize(user)
    return if user.nil?

    can :index, Board if user.kind_before_type_cast >= User.kinds[:assessor]
    can :show, Board if user.kind_before_type_cast >= User.kinds[:assessor] &&
                        user.board && user.board.id == :board_id

    can :read, user.board if user.kind_before_type_cast >= User.kinds[:assessor]

    can :manage, user.board if user.kind_before_type_cast >= User.kinds[:director]
  end
end
