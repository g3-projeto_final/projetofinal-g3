module UsersHelper

  def default_kind
    @user.kind = 'standard'
  end

  def default_board
    @user.board_id = 1
  end

  def user_kind_options
    [['Usuário Padrão', :standard],['Assessor', :assessor], ['Gerente', :manager], ['Diretor', :director]]
  end

  def translated_user_kind(user)
    if user.standard?
      'Usuário padrão'
    elsif user.assessor?
      'Assessor'
    elsif user.manager?
      'Gerente'
    elsif user.director?
      'Diretor'
    elsif user.admin?
      'Administrador'
    end
  end

  def user_board(user)
    if @user.board_id?
      @board_name = Board.find(@user.board_id)
      return @board_name.name
    end
  end
end
