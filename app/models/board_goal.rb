class BoardGoal < ApplicationRecord
  belongs_to :board

  has_many :board_goal_tasks

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }
  validates :board, presence: true
end
