class BoardTask < ApplicationRecord
  belongs_to :board

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }
  validates :board, presence: true
end
