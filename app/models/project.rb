class Project < ApplicationRecord
    has_many :develops
    has_many :developers, through: :develops, source: :user
end
