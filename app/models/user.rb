class User < ApplicationRecord
  has_many :joins
  has_many :cells, through: :joins

  has_many :develops
  has_many :project, through: :develops, source: :project

  has_many :forum_posts

  belongs_to :board

  mount_uploader :photo, PhotoUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
  before_save :default_values

  enum kind: {
    standard: 1,
    assessor: 2,
    manager: 3,
    director: 4,
    admin: 5
  }

  def default_values
    self.kind ||= 'standard'
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
