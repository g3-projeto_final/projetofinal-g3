class Board < ApplicationRecord
  has_many :users

  has_many :board_tasks, dependent: :destroy

  has_many :board_goals, dependent: :destroy

  has_many :user_boards
  has_many :users, through: :user_boards
end
