class Cell < ApplicationRecord
  has_many :joins
  has_many :members, through: :joins, source: :user
  belongs_to :board

  validates :name, presence: true, uniqueness: true
  validates :board, presence: true

  has_many :cell_tasks, dependent: :destroy
  has_many :cell_goals, dependent: :destroy
end
