class ForumPost < ApplicationRecord
  validates :title, presence: true, length: { minimum: 6 }
  validates :content, presence: true

  validates :user_id, presence: true

  belongs_to :user

  def number_of_likes
    likes = Like.where forum_post_id: id, kind: Like.kinds[:positive]
    likes.count
  end

  def number_of_dislikes
    dislikes = Like.where forum_post_id: id, kind: Like.kinds[:negative]
    dislikes.count
  end
end
