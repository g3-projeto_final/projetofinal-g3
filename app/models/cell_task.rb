class CellTask < ApplicationRecord
  belongs_to :cell

  validates :name, presence: true
  validates :cell, presence: true
end
