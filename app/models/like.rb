class Like < ApplicationRecord
  belongs_to :user
  belongs_to :forum_post

  enum kind: {
    positive: 1,
    negative: 2
  }
end
