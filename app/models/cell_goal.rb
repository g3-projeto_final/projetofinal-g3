class CellGoal < ApplicationRecord
  belongs_to :cell
  has_many :cell_goal_tasks, dependent: :destroy
end
