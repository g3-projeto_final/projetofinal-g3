# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_16_042422) do

  create_table "board_goal_tasks", force: :cascade do |t|
    t.integer "board_goal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_goal_id"], name: "index_board_goal_tasks_on_board_goal_id"
  end

  create_table "board_goals", force: :cascade do |t|
    t.integer "board_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "done"
    t.string "name"
    t.index ["board_id"], name: "index_board_goals_on_board_id"
  end

  create_table "board_tasks", force: :cascade do |t|
    t.integer "board_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "done", default: false
    t.string "name"
    t.index ["board_id"], name: "index_board_tasks_on_board_id"
  end

  create_table "boards", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cell_goal_tasks", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cell_goal_id"
    t.string "task_name"
    t.index ["cell_goal_id"], name: "index_cell_goal_tasks_on_cell_goal_id"
  end

  create_table "cell_goals", force: :cascade do |t|
    t.integer "cell_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "goal_name"
    t.index ["cell_id"], name: "index_cell_goals_on_cell_id"
  end

  create_table "cell_tasks", force: :cascade do |t|
    t.string "name"
    t.integer "cell_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "done"
    t.index ["cell_id"], name: "index_cell_tasks_on_cell_id"
  end

  create_table "cells", force: :cascade do |t|
    t.string "name"
    t.integer "board_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_id"], name: "index_cells_on_board_id"
  end

  create_table "develops", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_develops_on_project_id"
    t.index ["user_id"], name: "index_develops_on_user_id"
  end

  create_table "forum_posts", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_forum_posts_on_user_id"
  end

  create_table "joins", force: :cascade do |t|
    t.integer "user_id"
    t.integer "cell_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cell_id"], name: "index_joins_on_cell_id"
    t.index ["user_id"], name: "index_joins_on_user_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "forum_post_id"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["forum_post_id"], name: "index_likes_on_forum_post_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.text "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "manager"
  end

  create_table "user_boards", force: :cascade do |t|
    t.integer "user_id"
    t.integer "board_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_id"], name: "index_user_boards_on_board_id"
    t.index ["user_id"], name: "index_user_boards_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "photo"
    t.integer "board_id"
    t.index ["board_id"], name: "index_users_on_board_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
