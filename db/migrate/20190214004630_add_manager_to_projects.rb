class AddManagerToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :manager, :string
  end
end
