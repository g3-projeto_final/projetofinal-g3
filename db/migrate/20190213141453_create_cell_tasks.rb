class CreateCellTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :cell_tasks do |t|
      t.string :name
      t.references :cell, foreign_key: true

      t.timestamps
    end
  end
end
