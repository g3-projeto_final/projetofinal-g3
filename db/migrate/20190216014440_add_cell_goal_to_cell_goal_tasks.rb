class AddCellGoalToCellGoalTasks < ActiveRecord::Migration[5.2]
  def change
    add_reference :cell_goal_tasks, :cell_goal, foreign_key: true
  end
end
