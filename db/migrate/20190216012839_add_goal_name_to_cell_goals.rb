class AddGoalNameToCellGoals < ActiveRecord::Migration[5.2]
  def change
    add_column :cell_goals, :goal_name, :string
  end
end
