class AddNameToBoardTask < ActiveRecord::Migration[5.2]
  def change
    add_column :board_tasks, :name, :string
  end
end
