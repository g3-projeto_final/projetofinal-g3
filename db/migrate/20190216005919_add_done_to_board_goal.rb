class AddDoneToBoardGoal < ActiveRecord::Migration[5.2]
  def change
    add_column :board_goals, :done, :boolean
  end
end
