class CreateBoardGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :board_goals do |t|
      t.references :board, foreign_key: true

      t.timestamps
    end
  end
end
