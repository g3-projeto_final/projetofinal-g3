class RemoveCellGoalFromCellGoalTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :cell_goal_tasks, :cell_goal_id
  end
end
