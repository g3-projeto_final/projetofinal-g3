class RemoveFinishedFromCellTask < ActiveRecord::Migration[5.2]
  def change
    remove_column :cell_tasks, :finished, :boolean
  end
end
