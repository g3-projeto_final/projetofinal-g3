class CreateBoardTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :board_tasks do |t|
      t.references :board, foreign_key: true

      t.timestamps
    end
  end
end
