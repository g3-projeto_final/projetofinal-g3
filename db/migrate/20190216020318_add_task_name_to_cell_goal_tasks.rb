class AddTaskNameToCellGoalTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :cell_goal_tasks, :task_name, :string
  end
end
