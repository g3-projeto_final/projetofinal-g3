class AddStatusToBoardTask < ActiveRecord::Migration[5.2]
  def change
    add_column :board_tasks, :done, :boolean, default: false
  end
end
