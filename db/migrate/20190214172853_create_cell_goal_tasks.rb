class CreateCellGoalTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :cell_goal_tasks do |t|
      t.references :cell_goal, foreign_key: true

      t.timestamps
    end
  end
end
