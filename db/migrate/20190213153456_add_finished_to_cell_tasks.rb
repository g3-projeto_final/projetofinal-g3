class AddFinishedToCellTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :cell_tasks, :finished, :boolean
  end
end
