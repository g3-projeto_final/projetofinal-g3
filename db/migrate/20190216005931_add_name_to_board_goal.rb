class AddNameToBoardGoal < ActiveRecord::Migration[5.2]
  def change
    add_column :board_goals, :name, :string
  end
end
