class AddDoneToCellTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :cell_tasks, :done, :boolean
  end
end
