class CreateBoardGoalTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :board_goal_tasks do |t|
      t.references :board_goal, foreign_key: true

      t.timestamps
    end
  end
end
