# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Board.create name: 'projetos'
Board.create name: 'gestão de pessoas'

standard = User.new(first_name: 'Standard', last_name: 'Standard',
                    kind: User.kinds[:standard], email: 'standard@standard.com',
                    password: '123456', board: Board.first)
standard.save

assessor = User.new(first_name: 'Assessor', last_name: 'Assessor',
                    kind: User.kinds[:assessor], email: 'assessor@assessor.com',
                    password: '123456', board: Board.first)
assessor.save

manager = User.new(first_name: 'Manager', last_name: 'Manager',
                   kind: User.kinds[:manager], email: 'manager@manager.com',
                   password: '123456', board: Board.first)
manager.save

directorGP = User.new(first_name: 'DirectorGP', last_name: 'DirectorGP',
                    kind: User.kinds[:director], email: 'directorgp@directorgp.com',
                    password: '123456', board: Board.first)
directorGP.save

directorProjects = User.new(first_name: 'DirectorProjects', last_name: 'DirectorProjects',
                    kind: User.kinds[:director], email: 'directorprojects@directorprojects.com',
                    password: '123456', board: Board.second)
directorProjects.save

admin = User.new(first_name: 'Admin', last_name: 'Admin',
                 kind: User.kinds[:admin], email: 'admin@admin.com',
                 password: '123456', board: Board.first)
admin.save
