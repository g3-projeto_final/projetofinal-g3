Rails.application.routes.draw do
  root to: 'home#home'

  devise_for :users

  resources :cell_goal_tasks
  resources :cell_goals
  resources :board_goal_tasks
  # resources :board_goals
  resources :projects
  resources :cell_tasks
  resources :users, only: [:index, :show, :edit, :update, :destroy]
  resources :boards
  resources :cells
    


  # Tarefas de uma Diretoria
  get '/boards/:board_id/tasks/new', to: 'board_tasks#new', as: 'new_board_task'
  post '/boards/:board_id/tasks', to: 'board_tasks#create', as: 'board_tasks'
  get '/boards/:board_id/tasks/:task_id', to: 'board_tasks#show', as: 'board_task'
  patch '/boards/:board_id/tasks/:task_id', to: 'board_tasks#update'
  get '/boards/:board_id/tasks/:task_id/edit', to: 'board_tasks#edit', as: 'edit_board_task'

  # Metas de uma diretoria
  get '/boards/:board_id/goals/new', to: 'board_goals#new', as: 'new_board_goal'
  post '/boards/:board_id/goals', to: 'board_goals#create', as: 'board_goals'
  get '/boards/:board_id/goals/:goal_id', to: 'board_goals#show', as: 'board_goal'
  get '/boards/:board_id/goals/:goal_id/edit', to: 'board_goals#edit', as: 'edit_board_goal'
  patch '/boards/:board_id/goals/:goal_id', to: 'board_goals#update'

  namespace :forum do
    get '/', to: 'posts#index'
    get '/postagens', to: 'posts#index', as: 'posts'
    get '/postagens/novo', to: 'posts#new', as: 'new_post'
    post '/postagens', to: 'posts#create'

    get '/postagens/:id', to: 'posts#show', as: 'post'

    delete '/postagens/:id', to: 'posts#destroy'

    post '/postagens/:post_id/like', to: 'likes#create_like', as: 'add_like'
    post '/postagens/:post_id/dislike', to: 'likes#create_dislike', as: 'add_dislike'
  end
end
