require 'test_helper'

class CellTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cell_task = cell_tasks(:one)
  end

  test "should get index" do
    get cell_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_cell_task_url
    assert_response :success
  end

  test "should create cell_task" do
    assert_difference('CellTask.count') do
      post cell_tasks_url, params: { cell_task: { cell_id: @cell_task.cell_id, name: @cell_task.name } }
    end

    assert_redirected_to cell_task_url(CellTask.last)
  end

  test "should show cell_task" do
    get cell_task_url(@cell_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_cell_task_url(@cell_task)
    assert_response :success
  end

  test "should update cell_task" do
    patch cell_task_url(@cell_task), params: { cell_task: { cell_id: @cell_task.cell_id, name: @cell_task.name } }
    assert_redirected_to cell_task_url(@cell_task)
  end

  test "should destroy cell_task" do
    assert_difference('CellTask.count', -1) do
      delete cell_task_url(@cell_task)
    end

    assert_redirected_to cell_tasks_url
  end
end
