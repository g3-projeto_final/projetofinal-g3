require 'test_helper'

class CellGoalTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cell_goal_task = cell_goal_tasks(:one)
  end

  test "should get index" do
    get cell_goal_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_cell_goal_task_url
    assert_response :success
  end

  test "should create cell_goal_task" do
    assert_difference('CellGoalTask.count') do
      post cell_goal_tasks_url, params: { cell_goal_task: { cellgoal_id: @cell_goal_task.cellgoal_id } }
    end

    assert_redirected_to cell_goal_task_url(CellGoalTask.last)
  end

  test "should show cell_goal_task" do
    get cell_goal_task_url(@cell_goal_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_cell_goal_task_url(@cell_goal_task)
    assert_response :success
  end

  test "should update cell_goal_task" do
    patch cell_goal_task_url(@cell_goal_task), params: { cell_goal_task: { cellgoal_id: @cell_goal_task.cellgoal_id } }
    assert_redirected_to cell_goal_task_url(@cell_goal_task)
  end

  test "should destroy cell_goal_task" do
    assert_difference('CellGoalTask.count', -1) do
      delete cell_goal_task_url(@cell_goal_task)
    end

    assert_redirected_to cell_goal_tasks_url
  end
end
