require 'test_helper'

class BoardGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @board_goal = board_goals(:one)
  end

  test "should get index" do
    get board_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_board_goal_url
    assert_response :success
  end

  test "should create board_goal" do
    assert_difference('BoardGoal.count') do
      post board_goals_url, params: { board_goal: { board_id: @board_goal.board_id } }
    end

    assert_redirected_to board_goal_url(BoardGoal.last)
  end

  test "should show board_goal" do
    get board_goal_url(@board_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_board_goal_url(@board_goal)
    assert_response :success
  end

  test "should update board_goal" do
    patch board_goal_url(@board_goal), params: { board_goal: { board_id: @board_goal.board_id } }
    assert_redirected_to board_goal_url(@board_goal)
  end

  test "should destroy board_goal" do
    assert_difference('BoardGoal.count', -1) do
      delete board_goal_url(@board_goal)
    end

    assert_redirected_to board_goals_url
  end
end
