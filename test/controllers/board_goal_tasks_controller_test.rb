require 'test_helper'

class BoardGoalTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @board_goal_task = board_goal_tasks(:one)
  end

  test "should get index" do
    get board_goal_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_board_goal_task_url
    assert_response :success
  end

  test "should create board_goal_task" do
    assert_difference('BoardGoalTask.count') do
      post board_goal_tasks_url, params: { board_goal_task: { boardgoal_id: @board_goal_task.boardgoal_id } }
    end

    assert_redirected_to board_goal_task_url(BoardGoalTask.last)
  end

  test "should show board_goal_task" do
    get board_goal_task_url(@board_goal_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_board_goal_task_url(@board_goal_task)
    assert_response :success
  end

  test "should update board_goal_task" do
    patch board_goal_task_url(@board_goal_task), params: { board_goal_task: { boardgoal_id: @board_goal_task.boardgoal_id } }
    assert_redirected_to board_goal_task_url(@board_goal_task)
  end

  test "should destroy board_goal_task" do
    assert_difference('BoardGoalTask.count', -1) do
      delete board_goal_task_url(@board_goal_task)
    end

    assert_redirected_to board_goal_tasks_url
  end
end
