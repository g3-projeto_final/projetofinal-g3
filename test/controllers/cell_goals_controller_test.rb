require 'test_helper'

class CellGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cell_goal = cell_goals(:one)
  end

  test "should get index" do
    get cell_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_cell_goal_url
    assert_response :success
  end

  test "should create cell_goal" do
    assert_difference('CellGoal.count') do
      post cell_goals_url, params: { cell_goal: { cell_id: @cell_goal.cell_id } }
    end

    assert_redirected_to cell_goal_url(CellGoal.last)
  end

  test "should show cell_goal" do
    get cell_goal_url(@cell_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_cell_goal_url(@cell_goal)
    assert_response :success
  end

  test "should update cell_goal" do
    patch cell_goal_url(@cell_goal), params: { cell_goal: { cell_id: @cell_goal.cell_id } }
    assert_redirected_to cell_goal_url(@cell_goal)
  end

  test "should destroy cell_goal" do
    assert_difference('CellGoal.count', -1) do
      delete cell_goal_url(@cell_goal)
    end

    assert_redirected_to cell_goals_url
  end
end
