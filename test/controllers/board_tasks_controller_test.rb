require 'test_helper'

class BoardTasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @board_task = board_tasks(:one)
  end

  test "should get index" do
    get board_tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_board_task_url
    assert_response :success
  end

  test "should create board_task" do
    assert_difference('BoardTask.count') do
      post board_tasks_url, params: { board_task: { board_id: @board_task.board_id } }
    end

    assert_redirected_to board_task_url(BoardTask.last)
  end

  test "should show board_task" do
    get board_task_url(@board_task)
    assert_response :success
  end

  test "should get edit" do
    get edit_board_task_url(@board_task)
    assert_response :success
  end

  test "should update board_task" do
    patch board_task_url(@board_task), params: { board_task: { board_id: @board_task.board_id } }
    assert_redirected_to board_task_url(@board_task)
  end

  test "should destroy board_task" do
    assert_difference('BoardTask.count', -1) do
      delete board_task_url(@board_task)
    end

    assert_redirected_to board_tasks_url
  end
end
