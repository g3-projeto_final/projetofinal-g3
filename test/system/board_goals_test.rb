require "application_system_test_case"

class BoardGoalsTest < ApplicationSystemTestCase
  setup do
    @board_goal = board_goals(:one)
  end

  test "visiting the index" do
    visit board_goals_url
    assert_selector "h1", text: "Board Goals"
  end

  test "creating a Board goal" do
    visit board_goals_url
    click_on "New Board Goal"

    fill_in "Board", with: @board_goal.board_id
    click_on "Create Board goal"

    assert_text "Board goal was successfully created"
    click_on "Back"
  end

  test "updating a Board goal" do
    visit board_goals_url
    click_on "Edit", match: :first

    fill_in "Board", with: @board_goal.board_id
    click_on "Update Board goal"

    assert_text "Board goal was successfully updated"
    click_on "Back"
  end

  test "destroying a Board goal" do
    visit board_goals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Board goal was successfully destroyed"
  end
end
