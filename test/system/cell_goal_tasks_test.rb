require "application_system_test_case"

class CellGoalTasksTest < ApplicationSystemTestCase
  setup do
    @cell_goal_task = cell_goal_tasks(:one)
  end

  test "visiting the index" do
    visit cell_goal_tasks_url
    assert_selector "h1", text: "Cell Goal Tasks"
  end

  test "creating a Cell goal task" do
    visit cell_goal_tasks_url
    click_on "New Cell Goal Task"

    fill_in "Cellgoal", with: @cell_goal_task.cellgoal_id
    click_on "Create Cell goal task"

    assert_text "Cell goal task was successfully created"
    click_on "Back"
  end

  test "updating a Cell goal task" do
    visit cell_goal_tasks_url
    click_on "Edit", match: :first

    fill_in "Cellgoal", with: @cell_goal_task.cellgoal_id
    click_on "Update Cell goal task"

    assert_text "Cell goal task was successfully updated"
    click_on "Back"
  end

  test "destroying a Cell goal task" do
    visit cell_goal_tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cell goal task was successfully destroyed"
  end
end
