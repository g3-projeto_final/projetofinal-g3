require "application_system_test_case"

class CellGoalsTest < ApplicationSystemTestCase
  setup do
    @cell_goal = cell_goals(:one)
  end

  test "visiting the index" do
    visit cell_goals_url
    assert_selector "h1", text: "Cell Goals"
  end

  test "creating a Cell goal" do
    visit cell_goals_url
    click_on "New Cell Goal"

    fill_in "Cell", with: @cell_goal.cell_id
    click_on "Create Cell goal"

    assert_text "Cell goal was successfully created"
    click_on "Back"
  end

  test "updating a Cell goal" do
    visit cell_goals_url
    click_on "Edit", match: :first

    fill_in "Cell", with: @cell_goal.cell_id
    click_on "Update Cell goal"

    assert_text "Cell goal was successfully updated"
    click_on "Back"
  end

  test "destroying a Cell goal" do
    visit cell_goals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cell goal was successfully destroyed"
  end
end
