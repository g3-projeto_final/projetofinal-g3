require "application_system_test_case"

class BoardTasksTest < ApplicationSystemTestCase
  setup do
    @board_task = board_tasks(:one)
  end

  test "visiting the index" do
    visit board_tasks_url
    assert_selector "h1", text: "Board Tasks"
  end

  test "creating a Board task" do
    visit board_tasks_url
    click_on "New Board Task"

    fill_in "Board", with: @board_task.board_id
    click_on "Create Board task"

    assert_text "Board task was successfully created"
    click_on "Back"
  end

  test "updating a Board task" do
    visit board_tasks_url
    click_on "Edit", match: :first

    fill_in "Board", with: @board_task.board_id
    click_on "Update Board task"

    assert_text "Board task was successfully updated"
    click_on "Back"
  end

  test "destroying a Board task" do
    visit board_tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Board task was successfully destroyed"
  end
end
