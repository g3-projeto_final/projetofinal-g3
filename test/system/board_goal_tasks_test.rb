require "application_system_test_case"

class BoardGoalTasksTest < ApplicationSystemTestCase
  setup do
    @board_goal_task = board_goal_tasks(:one)
  end

  test "visiting the index" do
    visit board_goal_tasks_url
    assert_selector "h1", text: "Board Goal Tasks"
  end

  test "creating a Board goal task" do
    visit board_goal_tasks_url
    click_on "New Board Goal Task"

    fill_in "Boardgoal", with: @board_goal_task.boardgoal_id
    click_on "Create Board goal task"

    assert_text "Board goal task was successfully created"
    click_on "Back"
  end

  test "updating a Board goal task" do
    visit board_goal_tasks_url
    click_on "Edit", match: :first

    fill_in "Boardgoal", with: @board_goal_task.boardgoal_id
    click_on "Update Board goal task"

    assert_text "Board goal task was successfully updated"
    click_on "Back"
  end

  test "destroying a Board goal task" do
    visit board_goal_tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Board goal task was successfully destroyed"
  end
end
