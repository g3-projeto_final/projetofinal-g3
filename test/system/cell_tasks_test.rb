require "application_system_test_case"

class CellTasksTest < ApplicationSystemTestCase
  setup do
    @cell_task = cell_tasks(:one)
  end

  test "visiting the index" do
    visit cell_tasks_url
    assert_selector "h1", text: "Cell Tasks"
  end

  test "creating a Cell task" do
    visit cell_tasks_url
    click_on "New Cell Task"

    fill_in "Cell", with: @cell_task.cell_id
    fill_in "Name", with: @cell_task.name
    click_on "Create Cell task"

    assert_text "Cell task was successfully created"
    click_on "Back"
  end

  test "updating a Cell task" do
    visit cell_tasks_url
    click_on "Edit", match: :first

    fill_in "Cell", with: @cell_task.cell_id
    fill_in "Name", with: @cell_task.name
    click_on "Update Cell task"

    assert_text "Cell task was successfully updated"
    click_on "Back"
  end

  test "destroying a Cell task" do
    visit cell_tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cell task was successfully destroyed"
  end
end
